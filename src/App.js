import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./component/Login";
import Register from "./component/Register";
import "./App.css"
import Home from "./pages/Home";

function App() {
  return (
    <div className="App">
     <BrowserRouter>
     <main>
      <Switch>
        <Route path="/" component={Login} exact/>
        <Route path="/register" component={Register} exact/>
        <Route path="/home" component={Home} exact/>
      </Switch>
     </main>
     </BrowserRouter>
    </div>
  );
}

export default App;
