import axios from 'axios';
import React, { useState } from 'react';
import { Form, InputGroup} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState(""); 


    const history = useHistory();

    const login = async (e) =>{
        e.preventDefault();

        try {
            const {data, status} = await axios.post("http://localhost:3010/register/sign-in", {
                email: email,
                password: password,
            });
            if (status === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Login Berhasil!!!',
                    showConfirmButton: false,
                    timer: 1500
                });
                localStorage.setItem("userId", data.id);
                history.push("/home")
                window.location.reload();
            }
        } catch (error) {
            Swal.fire({
                icon: 'error',
                title: 'Username atau password tidak valid!',
                showConfirmButton: false,
                timer: 1500
            });
            console.log(error)
        }
    }

  return (
   <div className="kotak_login">
    <h1 className="mb-5">Login</h1>
    <Form onSubmit={login}>
        <div className="mb-3">
            <Form.Label>
                <strong>Email addres</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
                <Form.Control
                    placeholder="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
            </InputGroup>
        </div>
        <div className="mb-3">
            <Form.Label>
                <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
                <Form.Control
                    placeholder="Password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
            </InputGroup>
        </div>
        <button variant="primary" type="submit" className="mx-1 button btn">
            <i class="fas fa-sign-in-alt"></i>Login
        </button><br /><br />
        <center>
            <a href="/register">Registrasi</a>
            <br />
            <span >Jika belum memiliki akun</span>
        </center>
    </Form>
</div>
  )
}
