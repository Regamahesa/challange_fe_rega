import axios from 'axios';
import React, { useState } from 'react';
import { Form, InputGroup} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [username, setUsername] = useState("");

    const history = useHistory();

    const register = async (e) => {
        e.preventDefault();

        try {
            await axios.post("http://localhost:3010/register", {
                username: username,
                email: email,
                password: password,

            }).then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil Registrasi',
                    showConfirmButton: false,
                    timer: 1500
                })
                setTimeout(() => {
                    history.push('/')
                }, 1250)
            })
        } catch (error) {
            console.log(error)
        }
    }

  return (
    <div className="kotak_login">
    <h1 className="mb-5">Login</h1>
    <Form onSubmit={register}>
        <div className="mb-3">
            <Form.Label>
                <strong>Username</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
                <Form.Control
                    placeholder="username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                />
            </InputGroup>
        </div>
        <div className="mb-3">
            <Form.Label>
                <strong>Email addres</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
                <Form.Control
                    placeholder="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
            </InputGroup>
        </div>
        <div className="mb-3">
            <Form.Label>
                <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
                <Form.Control
                    placeholder="Password"
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
            </InputGroup>
        </div>
        <button variant="primary" type="submit" className="mx-1 button btn">
            <i class="fas fa-sign-in-alt"></i>Register
        </button><br /><br />
        <center>
            <a href="/">Login</a>
            <br />
            <span >Jika sudah memiliki akun</span>
        </center>
    </Form>
</div>
  )
}
